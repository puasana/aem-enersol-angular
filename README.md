# AEM Enersol Angular

This project is created as one of two assessments from AEM Enersol.

## How to set up this project

Clone this project from gitlab:
`git clone https://gitlab.com/puasana/aem-enersol-angular.git`

Install NPM Package:
`npm install`

Run Client:
`npm start`

Open in browser:
`http://localhost:4200`
