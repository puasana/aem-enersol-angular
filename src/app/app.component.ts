import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [
    '../../node_modules/bootstrap/scss/bootstrap.scss',
    './app.component.sass'
  ]
})
export class AppComponent {
  title = 'aem-enersol-angular';
}
