import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import '@angular/compiler';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { SignInComponent } from './auth/sign-in/sign-in.component';
// import { MainComponent } from './main/main.component';
// import { DashboardComponent } from './main/dashboard/dashboard.component';
// import { ChartDonutComponent } from './main/dashboard/chart-donut/chart-donut.component';
// import { ChartBarComponent } from './main/dashboard/chart-bar/chart-bar.component';
// import { UserListComponent } from './main/dashboard/user-list/user-list.component';
// import { ChartComponent } from './chart/chart.component';
// import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ChartsModule } from 'ng2-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainModule } from './main/main.module';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    SignInComponent,
    // MainComponent,
    // DashboardComponent,
    // ChartDonutComponent,
    // ChartBarComponent,
    // UserListComponent,
    // ChartComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    // NgxChartsModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MainModule
  ],
  schemas: [
    NO_ERRORS_SCHEMA,
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
