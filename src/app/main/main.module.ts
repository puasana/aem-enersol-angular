import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';
import { DashboardModule } from './dashboard/dashboard.module';

@NgModule({
  declarations: [
    MainComponent,
  ],
  imports: [
    CommonModule,
    DashboardModule
  ],
  providers: [],
  bootstrap: [MainComponent]
  
})
export class MainModule { }
