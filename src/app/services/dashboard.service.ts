import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { API_URL } from '../shared/api-url';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  constructor(
    private http: HttpClient
  ) {}

  getData(token: string): Observable<any[]> {
    const api = API_URL+ 'dashboard';
    const HttpOptions = {
      headers: new HttpHeaders({ 
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+ token
      })
    }

    return this.http.get<any[]>(api, HttpOptions);
  }
}
