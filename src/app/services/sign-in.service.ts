import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { API_URL } from '../shared/api-url';
import { Credential } from '../models/credential.model';

@Injectable({
  providedIn: 'root'
})
export class SignInService {
  constructor(
    private http: HttpClient
  ) {}

  checkCredential(params: Credential): Observable<Credential[]> {
    const api = API_URL+ 'account/login';
    const HttpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    }
    const data = {
      username: params.username === undefined ? '' : params.username,
      password: params.password === undefined ? '' : params.password
    }

    return this.http.post<Credential[]>(api, data, HttpOptions);
  }
}
